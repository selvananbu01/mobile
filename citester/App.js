/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import { Router, Scene, Actions, ActionConst, Drawer } from 'react-native-router-flux';

import Welcome from './src/Welcome';
import DashBoard from './src/DashBoard';


export default class App extends Component {
  render() {
    return (
      <Router> 
       <Scene key = "root">
             <Scene key="Welcome" component={Welcome} animation='fade' hideNavBar={true}  initial={true}    />
             <Scene key="DashBoard" component={DashBoard} animation='fade' hideNavBar={true}     />
        </Scene>
        </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
