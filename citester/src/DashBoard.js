import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { width, height, totalSize } from 'react-native-dimension';


export default class DashBoard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    
    onBackPressed(){
        Actions.Welcome();
    }
   componentDidMount(props){
       console.log('====================================');
       console.log(this.props.testID);
       console.log('====================================');
   }
    
    render() {
        return (
            <View style={styles.container} testID={this.props.testID}>
                <Text style={styles.welcome}> Welcome User </Text>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.onBackPressed()}>
                                <Text style={styles.textContainer}>
                                        Back
                                </Text>
                            </TouchableOpacity>
            </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    width:width(100),
    height:height(100)
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
    buttonContainer:{
        backgroundColor: '#a0185c',
        borderRadius: 25,
        height:height(7),
        width:width(25),
        justifyContent: 'center',
        alignItems: 'center',
        },
        textContainer:{
            fontSize: 24,
            color: '#FFF',
            textAlign: 'center',
            margin: 10,
            fontWeight: 'bold'
            
            }
});
