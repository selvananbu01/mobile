import React, { Component } from 'react'
import { Text, View,TouchableOpacity,StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { width, height, totalSize } from 'react-native-dimension';

export default class Welcome extends Component {

    OnGoPressed(){
                Actions.DashBoard({testID:"dashboard"});
    }
    render() {
        return (
            <View style={styles.container} testID="welcome">
                <Text style={styles.welcome}>Hope Your Having Good Time</Text>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.OnGoPressed()}>
                                <Text style={styles.textContainer} testID="GoButton">
                                        Go
                                </Text>
                            </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
    flex: 1,
    alignItems:"center",
    justifyContent:"center",
    width:width(100),
    height:height(100)
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
    textContainer:{
        fontSize: 24,
        color: '#FFF',
        textAlign: 'center',
        fontWeight: 'bold',
        margin: 10
        },
        buttonContainer:{
            backgroundColor: '#a0185c',
            borderRadius: 25,
            height:height(7),
            width:width(25),
            justifyContent: 'center',
            alignItems: 'center',
        },
})
